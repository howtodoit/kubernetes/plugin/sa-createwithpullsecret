## sa-createwithpullsecret k8s kubectl plugin

### Description
This plugin allows to create a ServiceAccount in specified namespace using docker registry credentials. If the space does not exist it will be created. Common use cases are:
- use the Service Account in your deployment manifest to allow pod to download from authenticated registry
- generate kubeconfig for this Service Account using other plugin genkubeconfig to generate kubeconfig file and allow kubernetes cluster connection in a Continuous Delivery scenario and also using kubectl config tool option following the principle of least privilege.


### Plugin Installation
```bash

# "install" your plugin by moving it to a directory in your $PATH 
mv ./sa-createwithpullsecret.sh /usr/local/bin/kubectl-sa-createwithpullsecret
chmod +x /usr/local/bin/kubectl-sa-createwithpullsecret

# check that kubectl recognizes your plugin
kubectl plugin list
```
### Plugin usage
Print usage info:
```bash 
kubectl sa createwithpullsecret --help
```
Create a Service Account with docker registry credentials in current namespace. The manifest will be saved in current path
```bash 
kubectl sa createwithpullsecret --service-account-name DUMMY-SA --registry-url DUMMY-DOCKER-SERVER --registry-username DUMMY-DOCKER-USERNAME --registry-password DUMMY-DOCKER-PASSWORD
```
Create a Service Account with docker registry credentials in current namespace. The manifest will be saved in the specified output path.
```bash 
kubectl sa createwithpullsecret --service-account-name DUMMY-SA --registry-url DUMMY-DOCKER-SERVER --registry-username DUMMY-DOCKER-USERNAME --registry-password DUMMY-DOCKER-PASSWORD --output-path 'home/user/manifest'
```
Create a Service Account with docker registry credentials and input namespace and log enabled:
```bash 
kubectl sa createwithpullsecret --service-account-name DUMMY-SA --namespace DUMMY-NAMESPACE --registry-url DUMMY-DOCKER-SERVER --registry-username DUMMY-DOCKER-USERNAME --registry-password DUMMY-DOCKER-PASSWORD --log-status on
```
Create a Service Account with docker registry credentials using input config context :
```bash 
kubectl sa createkubeconfig --service-account-name myserviceaccount --context mycontext
```

## Authors and acknowledgment
Vincenzo Santucci - vincenzo.santucci@gmail.com