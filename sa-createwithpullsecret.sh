#!/bin/bash
#ByVinx - Generate ServiceAccount with pull secret for namespace

###############################TEMPLATES TO APPLY########################
ROLE_TEMPLATE='{
    "apiVersion": "rbac.authorization.k8s.io/v1",
    "kind": "Role",
    "metadata": {
        "name": "<<sa.name>>",
        "namespace": "<<sa.namespace>>"
    },
    "rules": [
        {
            "apiGroups": ["*"],
            "resources": ["*"],
            "verbs": ["*"]
        }
    ]
}'

SERVICE_ACCOUNT_TEMPLATE='{
    "apiVersion": "v1",
    "kind": "ServiceAccount",
    "metadata": {
        "name": "<<sa.name>>",
        "namespace": "<<sa.namespace>>"
    },
    "automountServiceAccountToken": false,
    "imagePullSecrets": [
        {
            "name": "<<sa.name>>"
        }
    ]
}'

ROLE_BINDING_TEMPLATE='{
    "apiVersion": "rbac.authorization.k8s.io/v1",
    "kind": "RoleBinding",
    "metadata": {
        "name": "<<sa.name>>",
        "namespace": "<<sa.namespace>>"
    },
    "roleRef": {
        "apiGroup": "rbac.authorization.k8s.io",
        "kind": "Role",
        "name": "<<sa.name>>"
    },
    "subjects": [
        {
            "namespace": "<<sa.namespace>>",
            "kind": "ServiceAccount",
            "name": "<<sa.name>>"
        }
    ]
}'


#########################################################################
function usage {
    echo ""
    echo "Create a ServiceAccount in namespace with pull secret. You can use this ServiceAccount in your deployment manifest"
    echo ""
    echo "usage: $0 --service-account-name DUMMY_SA --namespace DUMMY_NAMESPACE --registry-url DUMMY_DOCKER_SERVER --registry-username DUMMY_DOCKER_USERNAME --registry-password DUMMY-DOCKER-PASSWORD --context MY-CONTEXT"
    echo ""
    echo "  --service-account-name  string   (Mandatory) - Name of Service account that will be created"
    echo "                                   (example: my-namespace-sa)"
    echo "  --registry-url string            (Mandatory) - Docker images registry Url"
    echo "                                   (example: - docker.io, registry.gitlab.myfactory.it)"
    echo "  --registry-username string       (Mandatory) - Docker images registry username"
    echo "                                   (example: myusr)"
    echo "  --registry-password string       (Mandatory) - Docker images registry password"
    echo "                                   (example: mypwd)"
    echo "  --context string                 (Optional) - Context to use to generate create service account"
    echo "                                   (example: - admin)"
    echo "  --namespace string               (Optional) - Namespace where service account will be created "
    echo "                                   (example: my-namespace) . If namespace does not exist it will be created!!"
    echo "  --output-path string             (Optional) - Path where the manifest files are created"
    echo "                                   (default: CURRENT_DIR/NAMESPACE_NAME/SERVICE_ACCOUNT_NAME/manifest/json)"
    echo "  --log-status string              (Optional) - default is off"
    echo "                                   (log-status on)"
    echo ""
}

function log {
    if [[ $LOG_STATUS == "on" ]] ; then
      echo "$1";
    fi
}

function die {
    printf "Script failed: %s\n\n" "$1"
    exit 1
}

#JQ IS REQUIRED - INSTALL IT BEFORE RUN SCRIPT
if ! [[ -x "$(command -v jq)" ]] ; then
    die "jq could not be found, please install before run this script"
fi

while [ $# -gt 0 ]; do
   if [[ $1 == "--help" ]]; then
        usage
        exit 0
    elif [[ $1 == "--"* ]]; then
        param_name="${1/--/}"
        parsed_param_name=${param_name//[-]/_} 
        declare "${parsed_param_name^^}"="$2"
        shift
    fi
    shift
done

#ServiceAccount for which generate the kubeconfig - Mandatory
if [[ -z "$SERVICE_ACCOUNT_NAME" ]] ; then
    usage
    die "Missing parameter --service_account_name"
else
    log "SERVICE ACCOUNT NAME: $SERVICE_ACCOUNT_NAME"
fi

if [[ -z "$REGISTRY_URL" ]] ; then
    usage
    die "REQUIRED DOCKER REGISTRY URL"
else
    log "USING INPUT DOCKER REGISTRY URL: $REGISTRY_URL"
fi

if [[ -z "$REGISTRY_USERNAME" ]] ; then
    usage
    die "REQUIRED DOCKER REGISTRY USERNAME"
else
    log "USING INPUT DOCKER REGISTRY USERNAME: $REGISTRY_USERNAME"
fi

if [[ -z "$REGISTRY_PASSWORD" ]] ; then
    usage
    die "REQUIRED DOCKER REGISTRY PASSWORD"
else
    log "USING INPUT DOCKER REGISTRY PASSWORD: $REGISTRY_PASSWORD"
fi

#Save original context before switch if needed
ORIGINAL_CURRENT_CONTEXT=$(kubectl config current-context)
#Context used to generate the kubeconfig -OPTIONAL (alternatively current-context is used)
if [[ -z "$CONTEXT" ]] ; then
    log "NO CONTEXT FOUND"
    log "USE CURRENT CONTEXT: "
    CONTEXT=$(kubectl config current-context)
    log $CONTEXT
else
    log "FOUND INPUT CONTEXT: $CONTEXT";
    kubectl config use-context $CONTEXT 1>/dev/null
    log "USE INPUT CONTEXT: $CONTEXT"
fi

if [[ -z "$NAMESPACE" ]] ; then
    log "NO INPUT NAMESPACE FOUND FOR SERVICE ACCOUNT: $SERVICE_ACCOUNT_NAME"
    NAMESPACE=$(kubectl get sa -o=jsonpath='{.items[0]..metadata.namespace}')
    log "USE CURRENT NAMESPACE: $NAMESPACE"
else
    log "USING INPUT NAMESPACE: $NAMESPACE"
fi

#CHECK NAMESPACE
NSCHECK=$(kubectl get namespace $NAMESPACE --ignore-not-found)
if [[ "$NSCHECK" ]]; then
    log "FOUND INPUT NAMESPACE: $NAMESPACE"
else
    log "CREATE INPUT NAMESPACE: $NAMESPACE"
    kubectl create namespace $NAMESPACE 1>/dev/null
fi

if [[ -z "$OUTPUT_PATH" ]] ; then
    log "NO OUTPUT_PATH FOUND, USE DEFAULT:"
    OUTPUT_PATH="$PWD/$NAMESPACE/$SERVICE_ACCOUNT_NAME/manifest/json"
    log $OUTPUT_PATH;
else
    log "FOUND OUTPATH - USE: $OUTPUT_PATH";
fi

mkdir -p $OUTPUT_PATH 1>/dev/null

#CREATE SECRET
kubectl -n $NAMESPACE create secret docker-registry "$SERVICE_ACCOUNT_NAME" --docker-server="$REGISTRY_URL" --docker-username="$REGISTRY_USERNAME" --docker-password="$REGISTRY_PASSWORD" 1>/dev/null

kubectl -n $NAMESPACE get secret "$SERVICE_ACCOUNT_NAME" -o 'json'| jq 'del(.metadata.annotations,.metadata.creationTimestamp,.metadata.managedFields,.metadata.resourceVersion,.metadata.selfLink,.metadata.uid,.status)'> "$OUTPUT_PATH/secret.json"

log "PARSING TEMPLATE TO CREATE KUBERNETES RESOURCE YAML FILES TO: $OUTPUT_PATH"

PARSED_ROLE=$(echo $ROLE_TEMPLATE | sed "s/<<sa.namespace>>/$NAMESPACE/g;s/<<sa.name>>/$SERVICE_ACCOUNT_NAME/g")
echo $PARSED_ROLE | jq '.' > "$OUTPUT_PATH/role.json"

PARSED_SERVICE_ACCOUNT=$(echo $SERVICE_ACCOUNT_TEMPLATE | sed "s/<<sa.namespace>>/$NAMESPACE/g;s/<<sa.name>>/$SERVICE_ACCOUNT_NAME/g")
echo $PARSED_SERVICE_ACCOUNT | jq '.' > "$OUTPUT_PATH/serviceaccount.json"

PARSED_ROLE_BINDING=$(echo $ROLE_BINDING_TEMPLATE | sed "s/<<sa.namespace>>/$NAMESPACE/g;s/<<sa.name>>/$SERVICE_ACCOUNT_NAME/g")
echo $PARSED_ROLE_BINDING | jq '.' > "$OUTPUT_PATH/rolebinding.json"

kubectl apply -f "$OUTPUT_PATH/serviceaccount.json" -f "$OUTPUT_PATH/role.json" -f "$OUTPUT_PATH/rolebinding.json" 1>/dev/null

log "RESTORE ORGINAL CURRENT CONTEXT: $ORIGINAL_CURRENT_CONTEXT"
kubectl config use-context $ORIGINAL_CURRENT_CONTEXT 1>/dev/null